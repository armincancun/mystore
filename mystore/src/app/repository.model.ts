import {Product} from './product.model';
import {SimpleDataSource} from './datasource.model';

export class Model {
    private datasource: SimpleDataSource;
    private products: Product[];
    private locator = (p: Product, id: number) => p.id === id;

    constructor() {
        this.datasource = new SimpleDataSource();
        this.products = new Array<Product>();
        this.datasource.getData().forEach(element => this.products.push(element));
    }

    getProducts(): Product[] {
        return this.products;
    }

    getProduct(id: number): Product {
        return this.products.find(p => this.locator(p, id));
    }

    saveProduct(product: Product) {
        if (product.id === 0 || product.id == null) {
            product.id = this.generateID();
            this.products.push(product);
        } else {
            const index = this.products.findIndex(p => this.locator(p, product.id));
            this.products.splice(index, 1, product);
        }
    }

    private generateID(): number {
        let candidate = 100;
        while (this.getProduct(candidate) !== null) {
            candidate++;
        }
        return candidate;
    }
}
